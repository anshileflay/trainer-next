module.exports = {
  use: [
    [
      '@neutrinojs/react',
      {
        html: {
          title: 'trainer2'
        }
      }
    ]
  ],
  watchOptions: {
    usePolling: true,
                interval: 500
    //poll: true // Check for changes every second
  },
  options: {
    host: '192.168.10.10',
    port: 3000,
    https: false,
    hot:true
  },
  notify: false,
  open: false
};

var dotenv = require('dotenv');
dotenv.load();