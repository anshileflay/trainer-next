import React, { Component } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
/*
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
*/

class ListQuest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listData : [],
            listText : 1,
            listIDs : [],
            listSection : 0,
            listTheme : 0,
        };
        //listIDs : props.listIDs,
        
        this.serverAPI = props.serverAPI;
        //this.listTest = props.listTest;
        //this.listText = props.list
        //this.listTest = [];
        //this.handleClick = this.handleClick.bind(this);
    }

    handleClick(id, e) {
        //e.preventDefault();
        //console.log(data);
        console.log(id);
        window.location = '?q='+id;
    }

    componentDidMount() {
        //this.setState({listSection : this.props.Section});
        //this.setState({listTheme : this.props.Theme});
        /*
        console.log('----element did mount');
        console.log(this.state.listSection);
        console.log(this.state.listTheme);
        console.log('----end');
        */
    }

    componentDidUpdate(prevProps) {
        //this.setState({ listText : prevProps.listText });
        //console.log('s '+this.props.Section);
        //console.log('t '+this.props.Theme);

        //console.log('s prev '+prevProps.Section);
        //console.log('t prev '+prevProps.Theme);
        var flag = false;
        if (this.props.Section !== prevProps.Section) {
            //this.fetchData(this.props.lisText);
            //this.setState({listText : this.props.listText});
            //console.log(this.serverAPI);
            //console.log('Section (list) : ' + this.props.Section);
            //console.log('Theme (list) : ' + this.props.Theme);
            this.setState({listSection : this.props.Section});
            flag = true;
        }
        if (this.props.Theme !== prevProps.Theme) {
            this.setState({listTheme : this.props.Theme});
            flag = true;
        }
        if (flag == true) this.getDataFromDB(this.props.Section, this.props.Theme);
        //console.log(flag);
        /*
        console.log('----element did update');
        console.log(this.state.listSection);
        console.log(this.state.listTheme);
        console.log('----end');
        */
    }

    getDataFromDB(sectionID, themeID) {
        let api_string = '';
        //console.log(this.state.listSection + '-' + this.state.listTheme);
        if (sectionID !== 0 && sectionID !== '') {
            api_string = 'api.php?type=list&s=' + sectionID
            if (themeID !== 0 && themeID !== '') {
                api_string += '&t=' + themeID;
            }
            //console.log(api_string);
            fetch(this.serverAPI + api_string)
                .then(response => response.json())
                .then(data => {
                    let colNames = [];
                    let colIDs = [];
                    for (let i = 0; i < data.length; i++) {
                        colIDs.push(data[i]['id_item']);
                        colNames.push(data[i]['name_item']);
                    }
                    this.setState({ listData: colNames});
                    this.setState({ listIDs: colIDs});
                });
        }
    }

    genDataTest() {
        let items = [];         
        var id = 0;
        for (let i = 0; i < this.state.listData.length; i++) {     
            id = this.state.listIDs[i]; 
            items.push(<ListItem onClick={this.handleClick.bind(this, id)} button key={i}><ListItemText primary={this.state.listData[i]} /></ListItem>);   
        }
        return items;
    }

    render() {
        return (
            <List component="nav">
                {this.genDataTest()}
            </List>
        );
    }
}

export default ListQuest;