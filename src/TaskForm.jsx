import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import ListQuest from './components/ListQuest/ListQuest';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Checkbox from '@material-ui/core/Checkbox';

const styles = theme => ({
	root: {
		display: 'flex',
		flexWrap: 'wrap',
		width: '100px',
	},
	formControl: {
		margin: theme.spacing.unit,
		minWidth: 120,
		whiteSpace: 'normal',
	},
	testClass: {
		whiteSpace: 'normal',
	},
	selectEmpty: {
		marginTop: theme.spacing.unit * 2,
	},
	mClass: {
		margin: 24,
	}
});

const API = 'http://192.168.10.10/test.php?query=';
const APIKEY = '5091fae1d5db65745aaa155afe536c1a';
//const SERVER = 'http://192.168.10.10/';
const SERVER = 'http://trainer.anshileflay.ru/public/';
const DEFAULT_QUERY = '';

class TaskForm extends Component {
	constructor(props) {
		super(props);
		//this.state = { value: '' };
		this.state = {
			taskID: '',
			taskName: '',
			taskText: '',
			taskAns: '',
			taskAnsVar: [],
			taskDif: '',
			taskTime: '',
			themeSelect: '',
			sectionSelect: '',
			varSelect: '',
			taskAnswer: '',
			themeSelectID: [],
			themeSelectDATA: [],
			sectionDataID: [],
			sectionDataName: [],
			varDataID: [],
			varDataName: [],
			itemsList: [],
			testI: '',
			name: '',
            shareholders: [],
		};

		this.taskTest = props.taskTest;
		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleChangeSection = this.handleChangeSection.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	componentDidMount() {
		//var tag = document.createElement('script');
		//tag.src = '//cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML';
		//document.getElementsByTagName('body').appendChild(tag);
		/*
		var aScript = document.createElement('script');
		aScript.type = 'text/javascript';
		aScript.async = true;
		aScript.src = "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML";

		document.head.appendChild(aScript);
		*/
		/*
		fetch(SERVER + 'api.php?type=theme')
			.then(response => response.json())
			.then(data => {
				console.log(data);
				let colNames = [];
				for (let i = 0; i < data.length; i++) {
					colNames.push(data[i]['name']);
				}
				this.setState({ themeSelectDATA: colNames});
				console.log(this.state.themeSelectDATA);
			});
		*/

			let url_string = window.location.href;
			let url = new URL(url_string);
			let itemID = url.searchParams.get("q");
			if (parseInt(itemID)) 
				itemID = parseInt(itemID);
			else 
				itemID = '';

			fetch(SERVER + 'api.php?type=section')
			.then(response => response.json())
			.then(data => {
				console.log(data);
				let colNames = [];
				let colIDs = [];
				for (let i = 0; i < data.length; i++) {
					colIDs.push(data[i]['id']);
					colNames.push(data[i]['name']);
				}
				this.setState({ sectionDataID: colIDs});
				this.setState({ sectionDataName: colNames});
				console.log(this.state.sectionDataID);
				console.log(this.state.sectionDataName);
			});
			fetch(SERVER + 'api.php?type=vars')
			.then(response => response.json())
			.then(data => {
				console.log(data);
				let colNames = [];
				let colIDs = [];
				for (let i = 0; i < data.length; i++) {
					colIDs.push(data[i]['id']);
					colNames.push(data[i]['name']);
				}
				this.setState({ varDataID: colIDs});
				this.setState({ varDataName: colNames});
				console.log(this.state.varDataID);
				console.log(this.state.varDataName);
			});
/*
			(() => {
				fetch(SERVER + 'api.php?type=vars')
				.then(response => response.json())
				.then(data => {
					console.log(data);
					let colNames = [];
					let colIDs = [];
					for (let i = 0; i < data.length; i++) {
						colIDs.push(data[i]['id']);
						colNames.push(data[i]['name']);
					}
					this.setState({ varDataID: colIDs});
					this.setState({ varDataName: colNames});
					console.log(this.state.varDataID);
					console.log(this.state.varDataName);
				});
			})();
			*/

			if (itemID != '')
			{
				fetch(SERVER + 'api.php?type=item&id='+itemID)
				.then(response => response.json())
				.then(data => {
					console.log(data);
					//тема - id_razdel
					//содержательный уровень - id_theme
					//трудность - difficulty
					//время решения - time_decision
					//текст задания - text_item
					//ответ - answer
					//название - name_item
					this.setState({ taskID: data['id_item']});
					this.setState({ sectionSelect: data['id_razdel']});
					this.setState({ taskName : data['name_item'] });
					this.setState({ taskDif : data['difficulty'] });
					this.setState({ taskTime : data['time_decision'] });
					this.setState({ taskText : data['text_item'] });
					this.setState({ taskAns : data['answer'] });
					this.setState({ taskAnsVar : data['ans_var'].split('<del>') });

					let themeID = data['id_theme'];
					let varID = data['type'];
					console.log(varID);
					if ( varID == '12' )
					{
						var checkedBuf = false;
						var ansBuf = data['answer'].split('<del>');

						for (var i = 0; i < this.state.taskAnsVar.length; i++)
						{
							if (ansBuf.indexOf( this.state.taskAnsVar[i] ) != -1)
								checkedBuf = true;
							else checkedBuf = false;
							this.setState({ shareholders: this.state.shareholders.concat([{ name: this.state.taskAnsVar[i], checked: checkedBuf }]) });
						}
					}

					fetch(SERVER + 'api.php?type=theme&params=' + data['id_razdel'])
						.then(response => response.json())
						.then(data => {
							//console.log(data);
							let colNames = [];
							let colIDs = [];
							for (let i = 0; i < data.length; i++) {
								colNames.push(data[i]['name']);
								colIDs.push(data[i]['id']);
							}
							this.setState({ themeSelectID: colIDs });
							this.setState({ themeSelectDATA: colNames });
							this.setState({ themeSelect :  themeID })
							this.setState({ varSelect : varID });
							//console.log(this.state.themeSelectDATA);
						});
				});

				//
			}
			
			//
	}

	handleInputChange(event) {
		this.setState({ value: event.target.value });
	}

	handleChange(event) {
		const target = event.target;
		const name = target.name;
		this.setState({ 
			[name]: event.target.value });
	}

	handleChangeM(event) {
		console.log(event.target.name);
		
		var name = parseInt(event.target.name.replace('ans', ''));

		console.log(name);
		var arr = [];
		console.log(this.state.taskText);
		arr[name] = event.target.value;
	}
    
    handleChangeArr(event, n) {
		const target = event.target;
		//const name = target.name;
		this.setState({ 
			testItems: event.target.value });
	}

	handleChangeSection(event) {
		console.log(typeof(event));
		const target = event.target;
		const name = target.name;
		const value = event.target.value;
		this.setState({ 
			[name]: event.target.value });
        
		fetch(SERVER + 'api.php?type=theme&params=' + value)
			.then(response => response.json())
			.then(data => {
				//console.log(data);
				let colNames = [];
				let colIDs = [];
				for (let i = 0; i < data.length; i++) {
					colNames.push(data[i]['name']);
					colIDs.push(data[i]['id']);
				}
				this.setState({ themeSelectDATA: colNames});
				this.setState({ themeSelectID: colIDs });
				//console.log(this.state.themeSelectDATA);
			});
	}

	handleSubmit(event) {  
		this.setState({ testI: "60" });
		console.log('Section :' + this.state.sectionSelect);
		console.log('Theme : ' + this.state.themeSelect);
		event.preventDefault();
	}

	createSelectItems() {
		let items = [];         
		for (let i = 0; i < this.state.themeSelectDATA.length; i++) {             
			items.push(<MenuItem style={{whiteSpace: 'normal'}} key={this.state.themeSelectID[i]} value={this.state.themeSelectID[i]}><p style={{whiteSpace: 'normal', margin: 0}}>{this.state.themeSelectDATA[i]}</p></MenuItem>);   
		}
		return items;
	} 

	getSectionDATA() {
		let items = [];         
		for (let i = 0; i < this.state.sectionDataID.length; i++) {             
			items.push(<MenuItem style={{whiteSpace: 'normal'}} key={this.state.sectionDataID[i]} value={this.state.sectionDataID[i]}>{this.state.sectionDataName[i]}</MenuItem>);
		}
		return items;
	} 

	getVarsDATA() {
		let items = [];         
		for (let i = 0; i < this.state.varDataID.length; i++) {             
			items.push(<MenuItem key={this.state.varDataID[i]} value={this.state.varDataID[i]}>{this.state.varDataName[i]}</MenuItem>);
		}
		return items;
	} 

	createSelectItemsTest() {
		let items = [];         
		for (let i = 0; i <= 10; i++) {             
			items.push(<MenuItem key={i} value={i}>--------------{i}</MenuItem>);   
		}
		return items;
	} 

	createVarsForm(typeVar) {
        let form = [];

        switch (typeVar) {
            case '9':
				//form.push(<TextField type="text" />);
				//var arr = this.state.taskAnsVar.split('<del>');
				
				//if ()
				//this.setState({ shareholders: this.state.shareholders.splice(0, 1, [{ name: '111111' }]) });
				console.log(this.state.shareholders);
				break;

			default:
				form.push(<TextField 
					id="taskAns" 
					name="taskAns"
					label="Ответ" 
					fullWidth
					value={this.state.taskAns} 
					onChange={this.handleChange}
				/>);
				break;
        }
		return form;
	}

		handleSave = () => {
			console.log('test');
			//id_item
			//name_item
			//text_item
			//ans_var
			//answer
			//type
			//difficulty
			//id_theme
			//time_decision
			//
			//console.log( this );

			let buffer = {};
			
			if ( this.state.taskID != '' ) buffer['id_item'] = this.state.taskID;

			buffer['id_razdel'] = this.state.sectionSelect;
			buffer['id_theme'] = this.state.themeSelect;
			buffer['difficulty'] = this.state.taskDif;
			buffer['time_decision'] = this.state.taskTime;
			buffer['text_item'] = this.state.taskText;
			buffer['type'] = this.state.varSelect;
			buffer['answer'] = this.state.taskAns;
			buffer['name_item'] = this.state.taskName;
			buffer['ans_var'] = '';

			var jsonData = JSON.stringify( buffer );
			console.log( jsonData );
			console.log( buffer );

			console.log (SERVER + 'api.php?type=add&key='+APIKEY+'&data=' +jsonData);

			fetch(SERVER + 'api.php?type=add&key='+APIKEY+'&data=' +jsonData)
				.then(response => response.json())
				.then(data => {
					console.log(data);
					if (data['status'] == 'added') {
						this.setState({ 'taskID' : data['id'] });
						console.log('added');
					}
					if (data['status'] == 'updated') {
						console.log('updated');
					}
					//else console.log( 'error' );
				});
	}
	
	/*--*/

	handleNameChange = (evt) => {
		this.setState({ name: evt.target.value });
	}

	handleShareholderNameChange = (idx) => (evt) => {
		const newShareholders = this.state.shareholders.map((shareholder, sidx) => {
			if (idx !== sidx) return shareholder;
			return { ...shareholder, name: evt.target.value };
		});

		this.setState({ shareholders: newShareholders });

		var arr = [];
		for (var i = 0; i < newShareholders.length; i++)
		{
			arr[i] = newShareholders[i]['name'];
		}
		this.setState({ taskAnsVar:  arr });
	}

	handleShareholderCheckedChange = (idx) => (evt) => {
		const newShareholders = this.state.shareholders.map((shareholder, sidx) => {
			if (idx !== sidx) return shareholder;
			return { ...shareholder, checked: evt.target.checked };
		});

		this.setState({ shareholders: newShareholders });

		var arr = [];
		for (var i = 0; i < newShareholders.length; i++)
		{
			if (newShareholders[i]['checked'] == true)
				arr.push(newShareholders[i]['name']);
		}
		this.setState({ taskAns:  arr.join('<del>') });
	}

	handleAddShareholder = () => {
		this.setState({ shareholders: this.state.shareholders.concat([{ name: '', checked: false }]) });
	}

	handleRemoveShareholder = (idx) => () => {
		console.log(idx);
		
		this.setState({ shareholders: this.state.shareholders.filter((s, sidx) => idx !== sidx) },
		() => {
			var buffer = [];
			for ( var i = 0; i < this.state.shareholders.length; i++)
			{
				if (this.state.shareholders[i].checked == true)
					buffer.push( this.state.shareholders[i].name );
			}
			this.setState({ taskAns: buffer.join('<del>') });
		}
		);
		this.setState({ taskAnsVar: this.state.taskAnsVar.filter((s, sidx) => idx !== sidx) });
		
		
	}
	/*
	typeSwitch(x) {
		if (x == 1) return 
			
	}*/

	/*--*/

	render() {
		return (
			<Grid container spacing={24}>
				<Grid item xs={6}>
					<form onSubmit={this.handleSubmit} autoComplete="off">
						<Grid>
							<FormControl fullWidth>
								<InputLabel shrink htmlFor="sectionSelect">
									Раздел
								</InputLabel>
								<Select 
									inputProps={{
										name: 'sectionSelect',
										id: 'sectionSelect',
									}}
									displayEmpty
									value={this.state.sectionSelect} 
									onChange={this.handleChangeSection}>
										{this.getSectionDATA()}
								</Select>
							</FormControl>
						</Grid>
						<br />
						<Grid>
							<FormControl fullWidth>
								<InputLabel shrink htmlFor="themeSelect">
									Тема
								</InputLabel>
								<Select 
									inputProps={{
										name: 'themeSelect',
										id: 'themeSelect',
									}}
									displayEmpty
									fullWidth
									style={{whiteSpace: 'normal'}}
									value={this.state.themeSelect} 
									onChange={this.handleChange}>
										{this.createSelectItems()}
								</Select>
							</FormControl>
						</Grid>
						<br />
						<Grid>
							<TextField 
								id="taskName" 
								name="taskName"
								label="Название" 
								fullWidth
								value={this.state.taskName} 
								onChange={this.handleChange}
							/>
						</Grid>
						<br />
						<Grid container spacing={8}>
							<Grid item xs={3}>
								<TextField 
									id="taskDif" 
									name="taskDif"
									label="Трудность" 
									fullWidth
									type="number"
									value={this.state.taskDif} 
									onChange={this.handleChange}
								/>
							</Grid>
							<Grid item xs={3}>
								<TextField 
									id="taskTime" 
									name="taskTime"
									label="Время решения (мин)" 
									fullWidth
									value={this.state.taskTime} 
									onChange={this.handleChange}
								/>
							</Grid>
							
						</Grid>
						<br />
						<Grid>
							<CKEditor
								editor={ ClassicEditor }
								data={this.state.taskText}
								onInit={ editor => {
										// You can store the "editor" and use when it's needed.
										console.log( 'Editor is ready to use!', editor );
								} }
								onChange={ ( event, editor ) => {
										const data = editor.getData();
										//console.log( { event, editor, data } );
										//console.log( data );
										this.setState({ taskText : data });
								} }
							/>
						</Grid>
						<br />
						<Grid item xs={6}>
							<FormControl fullWidth>
								<InputLabel shrink htmlFor="varSelect">
									Вариант проверки
								</InputLabel>
								<Select 
									inputProps={{
										name: 'varSelect',
										id: 'varSelect',
									}}
									autoWidth
									value={this.state.varSelect} 
									onChange={this.handleChange}>
										{this.getVarsDATA()}
								</Select>
							</FormControl>
						</Grid>
						<br />
						<Grid>
							{this.createVarsForm(this.state.varSelect)}
							<br /><br />
							<Grid>
								{this.state.taskAns}
								<br />
								<br />
								{this.state.taskAnsVar.map( function(num){
									return num + ' ';
								})}
							</Grid>
							<br />
							{this.state.shareholders.map((shareholder, idx) => (
							<div className="shareholder">
							<Checkbox 
								onClick={this.handleShareholderCheckedChange(idx)}
								checked={shareholder.checked}
							/>
							<TextField
								value={shareholder.name}
								onChange={this.handleShareholderNameChange(idx)}
								/>
							<Checkbox
								onClick={this.handleRemoveShareholder(idx)}
								indeterminate
							/>	
							</div>
							))}
							<button type="button" onClick={this.handleAddShareholder} className="small">Add Shareholder</button>
						</Grid>

						<br /><br /><br /><br />
						<Grid 
							container
							alignItems="flex-end"
						>
							<Grid item>
								<Button onClick={this.handleSave} variant="contained" color="primary">
									Сохранить
								</Button>
							</Grid>
						</Grid>
					</form>
					
				</Grid>
				<Grid item xs={6}>
					<ListQuest serverAPI={SERVER} Section={this.state.sectionSelect} Theme={this.state.themeSelect}/>
				</Grid>
			</Grid>
		);
	}
}

/*
<label>
	Текст задания<br />
	<textarea name="taskText" value={this.state.taskText} onChange={this.handleChange}></textarea>
</label>
*/

export default withStyles(styles)(TaskForm);