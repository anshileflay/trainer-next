import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import TaskForm from './TaskForm';

var arr = new Array();
var strTest = 'some';

const root = document.getElementById('root');

const load = () => render((
  <Card>
    <CardContent>
      <AppContainer>
        <TaskForm taskTest={strTest}/>
      </AppContainer>
    </CardContent>
  </Card>
), root);

// This is needed for Hot Module Replacement
if (module.hot) {
  module.hot.accept('./TaskForm', load);
  module.hot.accept('./components/ListQuest/ListQuest');
}

load();
